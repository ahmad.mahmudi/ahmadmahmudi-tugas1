import React from 'react';
import {View, Text, Image, StyleSheet, TextInput, TouchableOpacity} from 'react-native';

const FormLogin = () => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View>
          <View  style={styles.digtlapp}>
            <Text style={styles.digtlapptext}>Digital Approval</Text>
          </View>
        </View>
        <Image source={require('./gambar.png')} style={styles.image}/>
        <TextInput placeholder='Alamat Email' style={styles.input1}/>
        <TextInput placeholder='Password' style={styles.input2}/>
        <View style={styles.rstpwd}>
          <Text style={styles.rstpw}>Reset Password</Text>
        </View>
        <TouchableOpacity style={styles.login}>
          <Text style={styles.buttontext}>
            LOGIN
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  digtlapptext: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 5,
  },
  image: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginBottom: 25,
    marginTop: -15,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F7F7F7',
  },
  content: {
    width: '90%',
    height: '70%',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    alignContent: 'center',
    borderRadius: 20,
  },
  login: {
    width: 300,
    height: 48,
    backgroundColor: '#287AE5',
    alignSelf: 'center',
    justifyContent: 'center',
    marginBottom: -40,
    borderRadius: 10,
  },

  buttontext:{
    alignSelf: 'center',
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 16,
  },

  digtlapp:{
    backgroundColor: '#002558',
    borderRadius: 20,
    width: 170,
    height: 36,
    alignSelf: 'center',
    marginBottom: 20,
    marginTop: -90,
  },
  rstpwd: {
    marginLeft: 220,
    marginBottom: 24,
    marginTop: 16,
  },
  rstpw:{
    color: '#287AE5',
    fontSize: 16,
    fontStyle: 'italic',
  },
  input1:{
    width: '80%',
    alignSelf: 'center',
    borderWidth: 0.2,
    padding: 10,
    borderRadius: 5,
    marginTop: 30,
    marginBottom: 12,
  },
  input2:{
    width: '80%',
    alignSelf: 'center',
    borderWidth: 0.2,
    padding: 10,
    borderRadius: 5,
    marginTop: 12,
  }
})
export default FormLogin;